@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far {{($parent_id==0)?'active':''}}"><a href="{{route('adminUserPermissions', [0])}}">Functions</a></li>
    @foreach($permission_parents as $p)
    <li class="breadcrumb-item far {{($parent_id==$p->id)?'active':''}}"><a href="{{route('adminUserPermissions', [$p->id])}}">{{ $p->name }}</a></li>
    @endforeach
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>
        @if($parent_id > 0)
         Sub Functions</i></b>
        @else
        User Functions
        @endif
  
        -  
        
        @if($parent_id > 0)
        <i><b>{{ $parent_title }} Level</b></i>
        @else
        Root Level
        @endif
    </h1>
    <div class="header-actions">
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
            data-mdc-auto-init="MDCRipple"
            href="{{route('adminUserPermissionsCreate', [$parent_id])}}"
        >
            Create
        </a>
    </div>
</header>
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-body">
        @if(count($permissions) > 0)
        {!! Form::open(['route'=>'adminUserPermissionsDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
        <input type="hidden" name="category" value="{{$parent_id}}">
            <table class="caboodle-table">
              <thead>
                <tr>
                  <th></th>
                  <th width="50px">
                    <div class="mdc-form-field" data-toggle="tooltip" title="Select All">
                        <div class="mdc-checkbox caboodle-table-select-all">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="select_all" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                    </div>
                  </th>
                  <th class="caboodle-table-col-action">
                    <a class="caboodle-btn caboodle-btn-icon caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated x-small uppercase" 
                            data-mdc-auto-init="MDCRipple"
                            href="{{ route('adminUserPermissionsDestroy') }}"
                            method="DELETE"
                            data-toggle-alert="warning"
                            data-alert-form-to-submit=".form-delete"
                            permission-action="delete"
                            data-notif-message="Deleting {{$title}}">
                        <i class="fas fa-trash"></i>
                    </a>
                  </th>
                  <th class="caboodle-table-col-header hide" >Name</th>
                  <th class="caboodle-table-col-header hide" >Description</th>
                    
                  <th colspan="100%" ></th>
                </tr>
              </thead>
              <tbody id="sortableCategory" >
                @foreach($permissions as $d)
                  <tr id="{{$d->id}}" >
                    <td><i class="far fa-bars sortable-icon"></i></td>
                    <td>
                      <div class="mdc-form-field">
                        <div class="mdc-checkbox">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                      </div>
                    </td>
                    <td ><a href="{{route('adminUserPermissions', [$d->id])}}">{{$d->name}}</a></td>
                    <td class="uppercase sub-text-1" >{{$d->description}}</td>
                     
                    <td width="110px" class="text-center">
                      @if (Auth::user()->type == 'super')
                        <a href="{{route('adminUserPermissionsEdit', [$d->id])}}" 
                            class="mdc-icon-toggle animated-icon" 
                            data-toggle="tooltip"
                            title="Manage"
                            role="button"
                            aria-pressed="false"
                            permission-action="edit">
                            <i class="far fa-edit" aria-hidden="true"></i>
                        </a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          {!! Form::close() !!}
        @else
          <div class="empty text-center">
              No results found
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
@stop

@section('added-scripts')
  <script>
    var categoryTable = $('#categoryTable');
    var parent = '{{$parent_id}}';
    $( "#sortableCategory" ).sortable({
      update: function( event, ui ) {
        var newOrderArr = $(this).sortable('toArray');
        console.log(newOrderArr);
        reorderCategory(newOrderArr);
      }
    });

    function reorderCategory(order)
    {
      $.ajax({
        type : 'GET',
        url: "{{route('adminUserPermissionsOrder')}}",
        data : {
          'order': order,
          'parent': parent
        },
        dataType: 'json',
        success : function(data) {
          console.log(data);
        },
        error : function(data, text, error) {

        }
      });
    }
  </script>
@stop
